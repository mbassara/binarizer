package pl.edu.uj.zfm.bmpbinarizer.config;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {

	public static final String IN_DIR_PROPERTY = "bmpbinarizer.in.dir.path";

	public static final String OUT_DIR_PROPERTY = "bmpbinarizer.out.dir.path";

	public static final String AVG_PROPERTY = "bmpbinarizer.avg";

	public static final String SIGMA_PROPERTY = "bmpbinarizer.sigma";

	public static final String FIRST_CRITERIUM_ONLY_PROPERTY = "bmpbinarizer.first.criterium.only";

	private static final Config INSTANCE = new Config();

	public static Config getInstance() {
		return INSTANCE;
	}

	private static Properties getDefaults() {
		Properties properties = new Properties();
		properties.put(IN_DIR_PROPERTY, System.getProperty("user.home"));
		properties.put(OUT_DIR_PROPERTY, System.getProperty("user.home"));
		properties.put(AVG_PROPERTY, "128");
		properties.put(SIGMA_PROPERTY, "20");
		properties.put(FIRST_CRITERIUM_ONLY_PROPERTY, "false");
		return properties;
	}

	private final File configFile;

	private final Properties properties = new Properties(getDefaults());

	private Config() {
		String configParent = System.getProperty("user.home");
		if (SystemUtils.IS_OS_WINDOWS) {
			String appdata = System.getenv("APPDATA");
			configParent = appdata != null ? appdata : configParent;
		}
		File configDir = new File(configParent, ".BmpBinarizer");
		if (!configDir.exists()) {
			configDir.mkdirs();
		}
		configFile = new File(configDir, "config.properties");
		if (configFile.exists()) {
			FileInputStream configStream = null;
			try {
				configStream = new FileInputStream(configFile);
				properties.load(configStream);
			} catch (IOException e) {
				System.err.println("Cannot load config from file.");
				e.printStackTrace();
			} finally {
				IOUtils.closeQuietly(configStream);
			}
		}
	}

	public File getInDirectory() {
		return new File(INSTANCE.properties.getProperty(IN_DIR_PROPERTY));
	}

	public void setInDirectory(File inDirectory) {
		INSTANCE.properties.setProperty(IN_DIR_PROPERTY, inDirectory.getAbsolutePath());
	}

	public File getOutDirectory() {
		return new File(INSTANCE.properties.getProperty(OUT_DIR_PROPERTY));
	}

	public void setOutDirectory(File outDirectory) {
		INSTANCE.properties.setProperty(OUT_DIR_PROPERTY, outDirectory.getAbsolutePath());
	}

	public int getAvg() {
		return Integer.valueOf(INSTANCE.properties.getProperty(AVG_PROPERTY));
	}

	public void setAvg(int avg) {
		INSTANCE.properties.setProperty(AVG_PROPERTY, avg + "");
	}

	public int getSigma() {
		return Integer.valueOf(INSTANCE.properties.getProperty(SIGMA_PROPERTY));
	}

	public void setSigma(int sigma) {
		INSTANCE.properties.setProperty(SIGMA_PROPERTY, sigma + "");
	}

	public boolean getFirstCriteriumOnly() {
		return Boolean.valueOf(INSTANCE.properties.getProperty(FIRST_CRITERIUM_ONLY_PROPERTY));
	}

	public void setFirstCriteriumOnly(boolean firstCriteriumOnly) {
		INSTANCE.properties.setProperty(FIRST_CRITERIUM_ONLY_PROPERTY, firstCriteriumOnly + "");
	}

	public void save() {
		try {
			properties.store(new FileOutputStream(configFile), null);
		} catch (IOException e) {
			System.err.println("Cannot save config to file.");
			e.printStackTrace();
		}
	}
}
