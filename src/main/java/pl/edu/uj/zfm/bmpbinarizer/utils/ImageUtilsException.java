package pl.edu.uj.zfm.bmpbinarizer.utils;

public class ImageUtilsException extends Exception {
	private static final long serialVersionUID = 1L;

	public ImageUtilsException(String message, Throwable cause) {
		super(message, cause);
	}

	public ImageUtilsException(String message) {
		super(message);
	}

	public ImageUtilsException(Throwable cause) {
		super(cause);
	}

}
