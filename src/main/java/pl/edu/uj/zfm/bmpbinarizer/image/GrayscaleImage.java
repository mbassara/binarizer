package pl.edu.uj.zfm.bmpbinarizer.image;

import pl.edu.uj.zfm.bmpbinarizer.utils.ImageUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GrayscaleImage {

    private BufferedImage image;

    GrayscaleImage(File file) throws IOException {
        this(ImageUtils.read(file));
    }

    private GrayscaleImage(BufferedImage image) {
        this.image = image;
    }

    public GrayscaleImage(BufferedImage image, int width, int height) {
        this.image = ImageUtils.resize(image, width, height);
    }

    public int getPixel(int x, int y) {
        int rgb = image.getRGB(x, y);
        int red = (rgb >> 16) & 0x000000FF;
        int green = (rgb >> 8) & 0x000000FF;
        int blue = (rgb) & 0x000000FF;

        return max(red, green, blue);
    }

    private int max(int red, int green, int blue) {
        return Math.max(Math.max(red, green), Math.max(green, blue));
    }

    public void setPixel(int x, int y, int value) {
        int alpha = 255 << 24;
        int red = value << 16;
        int green = value << 8;
        //noinspection UnnecessaryLocalVariable
        int blue = value;

        int rgb = alpha + red + green + blue;
        image.setRGB(x, y, rgb);
    }

    public int getHeight() {
        return image.getHeight();
    }

    public int getWidth() {
        return image.getWidth();
    }

    public BufferedImage getImage() {
        return image;
    }

    public GrayscaleImage copy() {
        BufferedImage image = ImageUtils.clone(getImage());
        return new GrayscaleImage(image);
    }

    public void save(File file) throws IOException {
        ImageUtils.write(image, file);
    }

}
