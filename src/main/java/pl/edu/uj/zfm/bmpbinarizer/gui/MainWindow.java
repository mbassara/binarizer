package pl.edu.uj.zfm.bmpbinarizer.gui;

import pl.edu.uj.zfm.bmpbinarizer.config.Config;
import pl.edu.uj.zfm.bmpbinarizer.image.Binarizer;
import pl.edu.uj.zfm.bmpbinarizer.image.BinarizerException;
import pl.edu.uj.zfm.bmpbinarizer.image.GrayscaleImage;
import pl.edu.uj.zfm.bmpbinarizer.utils.ImageUtils;
import pl.edu.uj.zfm.bmpbinarizer.utils.ResourcesUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MainWindow extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final String ICON_RESOURCE_PATH = "/icon.png";
	private static final int VER_GAP = 5;
	private static final int HOR_GAP = 10;

	private final Config config = Config.getInstance();

	private JLabel inputFilesInfoLabel;
	private JTextField outputDirTextField;
	private JLabel imagePreviewLabel;
	private JTextField avgTextField;
	private JTextField stdDevTextField;
	private JCheckBox firstCriteriumOnlyCheckBox;
	private JSlider avgSlider;
	private JSlider sSlider;

	private File[] selectedFiles;
    private GrayscaleImage topPreviewImage;
    private GrayscaleImage midPreviewImage;
    private GrayscaleImage bottomPreviewImage;

	private ParametersChangeListener parametersChangeListener = new ParametersChangeListener();
	private ProgressListener progressListener;

	public static void main(String[] args) {
		new MainWindow().setVisible(true);
	}

    private MainWindow() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				config.save();
			}
		});

		setTitle("Bitmap binarizer");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		JPanel contentPanel = new JPanel();
		contentPanel.setBorder(BorderFactory.createEmptyBorder(VER_GAP, HOR_GAP, VER_GAP, HOR_GAP));
		setContentPane(contentPanel);

		setLayout(new BorderLayout(HOR_GAP, VER_GAP));
		setPreferredSize(new Dimension(500, 600));
		setResizable(false);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			setIconImage(ImageIO.read(getClass().getResourceAsStream(ICON_RESOURCE_PATH)));
		} catch (Exception e) {
			e.printStackTrace();
		}

		setupTopComponents();
		setupCenterComponents();
		setupBottomComponents();

		pack();
	}

	private void setupTopComponents() {
		inputFilesInfoLabel = new JLabel("No input files chosen.");
		inputFilesInfoLabel.setVerticalTextPosition(JLabel.BOTTOM);
		outputDirTextField = new JTextField(config.getOutDirectory().getAbsolutePath());

		JButton inputFilesButton = new JButton("Input files");
		inputFilesButton.addActionListener(inputFilesButtonListener);

		JButton sampleFilesButton = new JButton("Sample");
		sampleFilesButton.addActionListener(sampleFilesButtonListener);

		JButton outputDirButton = new JButton("Output directory");
		outputDirButton.addActionListener(outputDirectoryButtonListener);

		JPanel textPanel = new JPanel(new BorderLayout(HOR_GAP, VER_GAP));
		textPanel.add(inputFilesInfoLabel, BorderLayout.PAGE_START);
		textPanel.add(outputDirTextField, BorderLayout.PAGE_END);

		JPanel inputButtonsPanel = new JPanel(new BorderLayout());
		inputButtonsPanel.add(inputFilesButton, BorderLayout.CENTER);
		inputButtonsPanel.add(sampleFilesButton, BorderLayout.LINE_END);

		JPanel buttonsPanel = new JPanel(new BorderLayout(HOR_GAP, VER_GAP));
		buttonsPanel.add(inputButtonsPanel, BorderLayout.PAGE_START);
		buttonsPanel.add(outputDirButton, BorderLayout.PAGE_END);

		JPanel topPanel = new JPanel(new BorderLayout(HOR_GAP, VER_GAP));
		topPanel.add(textPanel, BorderLayout.CENTER);
		topPanel.add(buttonsPanel, BorderLayout.LINE_END);

		JProgressBar progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setString("");
		progressListener = new ProgressListener(progressBar);
		topPanel.add(progressBar, BorderLayout.PAGE_END);

		add(topPanel, BorderLayout.PAGE_START);
	}

	private void setupBottomComponents() {
		avgTextField = new JTextField(config.getAvg() + "");
		avgTextField.setPreferredSize(new Dimension(40, 0));
		avgTextField.setInputVerifier(numberInputVerifier);
		avgTextField.addKeyListener(parametersChangeListener);

		stdDevTextField = new JTextField(config.getSigma() + "");
		stdDevTextField.setPreferredSize(new Dimension(40, 0));
		stdDevTextField.setInputVerifier(numberInputVerifier);
		stdDevTextField.addKeyListener(parametersChangeListener);

		avgSlider = new JSlider(0, 255, config.getAvg());
		sSlider = new JSlider(0, 100, config.getSigma());

		avgSlider.addChangeListener(parametersChangeListener);
		sSlider.addChangeListener(parametersChangeListener);

		firstCriteriumOnlyCheckBox = new JCheckBox("First criterium only", config.getFirstCriteriumOnly());
		firstCriteriumOnlyCheckBox.addChangeListener(parametersChangeListener);

		JPanel topSliderInnerPanel = new JPanel(new BorderLayout());
		topSliderInnerPanel.add(firstCriteriumOnlyCheckBox, BorderLayout.PAGE_START);
		topSliderInnerPanel.add(avgSlider, BorderLayout.CENTER);
		topSliderInnerPanel.add(avgTextField, BorderLayout.LINE_END);

		JPanel bottomSliderInnerPanel = new JPanel(new BorderLayout());
		bottomSliderInnerPanel.add(sSlider, BorderLayout.CENTER);
		bottomSliderInnerPanel.add(stdDevTextField, BorderLayout.LINE_END);

		JPanel topSliderPanel = new JPanel(new BorderLayout());
		topSliderPanel.add(new JLabel("μ = "), BorderLayout.LINE_START);
		topSliderPanel.add(topSliderInnerPanel, BorderLayout.CENTER);

		JPanel bottomSliderPanel = new JPanel(new BorderLayout());
		bottomSliderPanel.add(new JLabel("σ = "), BorderLayout.LINE_START);
		bottomSliderPanel.add(bottomSliderInnerPanel, BorderLayout.CENTER);

		JPanel slidersPanel = new JPanel(new BorderLayout());
		slidersPanel.add(topSliderPanel, BorderLayout.PAGE_START);
		slidersPanel.add(bottomSliderPanel, BorderLayout.PAGE_END);

		JButton binarizeImagesButton = new JButton("Binarize images");
		binarizeImagesButton.addActionListener(binarizeImagesButtonListener);

		JButton resetPreviewButton = new JButton("Reset preview");
		resetPreviewButton.addActionListener(resetButtonListener);

		JPanel binarizeImagesButtonPanel = new JPanel(new GridBagLayout());
		binarizeImagesButtonPanel.add(binarizeImagesButton);
		binarizeImagesButtonPanel.add(resetPreviewButton);

		JPanel bottomPanel = new JPanel(new BorderLayout());
		bottomPanel.add(slidersPanel, BorderLayout.PAGE_START);
		bottomPanel.add(binarizeImagesButtonPanel, BorderLayout.PAGE_END);

		add(bottomPanel, BorderLayout.PAGE_END);
	}

	private void setupCenterComponents() {
		imagePreviewLabel = new JLabel("", JLabel.CENTER);
		add(imagePreviewLabel, BorderLayout.CENTER);
	}

	private void setPreviewImage(File topFile, File midFile, File bottomFile) {
		try {
			BufferedImage topImage = ImageUtils.read(topFile);
			BufferedImage midImage = ImageUtils.read(midFile);
			BufferedImage bottomImage = ImageUtils.read(bottomFile);
            topPreviewImage = new GrayscaleImage(topImage, imagePreviewLabel.getWidth(), imagePreviewLabel.getHeight());
            midPreviewImage = new GrayscaleImage(midImage, imagePreviewLabel.getWidth(), imagePreviewLabel.getHeight());
            bottomPreviewImage = new GrayscaleImage(bottomImage, imagePreviewLabel.getWidth(), imagePreviewLabel.getHeight());
			imagePreviewLabel.setIcon(new ImageIcon(midPreviewImage.getImage()));
		} catch (IOException e) {
			showError(String.format("Cannot read one of following files: %s, %s, %s. %s", topFile, midFile, bottomFile,
					e.getMessage()));
		}
	}

	private void refreshPreview(Component focusComponent, final Container baseContainerToDisable, final int avg,
			final int stdDev, final boolean firstCriteriumOnly) {
		if (topPreviewImage != null && midPreviewImage != null && bottomPreviewImage != null) {
			runLongTask(new Runnable() {
				@Override
				public void run() {
					try {
                        GrayscaleImage image = Binarizer.binarize(topPreviewImage, midPreviewImage, bottomPreviewImage, avg,
								stdDev, firstCriteriumOnly);
						imagePreviewLabel.setIcon(new ImageIcon(image.getImage()));
					} catch (BinarizerException e) {
						showError("Cannot draw preview. " + e.getMessage());
					}
				}
			}, focusComponent, baseContainerToDisable);
		}
	}

	private void inputFilesSelected(File[] selectedFiles) {
		this.selectedFiles = selectedFiles;
		int length = selectedFiles.length;
		if (length > 0) {
			if (!ResourcesUtils.isResource(selectedFiles[0])) {
				config.setInDirectory(selectedFiles[0].getParentFile());
			}
			inputFilesInfoLabel.setText(String.format("%d file%s chosen.", length, (length > 1) ? "s" : ""));
			setPreviewImage(selectedFiles[0], selectedFiles[1], selectedFiles[2]);
		} else {
			inputFilesInfoLabel.setText("No input files chosen.");
		}
	}

	private void showError(String message) {
		JOptionPane.showMessageDialog(MainWindow.this, message, "Error", JOptionPane.ERROR_MESSAGE);
	}

	private void showInfo(String message) {
		JOptionPane.showMessageDialog(MainWindow.this, message, "Information", JOptionPane.INFORMATION_MESSAGE);
	}

	private void setComponentsEnabled(Container container, boolean enabled) {
		Component[] components = container.getComponents();
		for (Component component : components) {
			component.setEnabled(enabled);
			if (component instanceof Container) {
				setComponentsEnabled((Container) component, enabled);
			}
		}
	}

	private void runLongTask(final Runnable runnable, final Component focusComponent, final Container baseContainerToDisable) {
		if (baseContainerToDisable != null) {
			setComponentsEnabled(baseContainerToDisable, false);
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					runnable.run();
				} finally {
					if (baseContainerToDisable != null) {
						setComponentsEnabled(baseContainerToDisable, true);
					}
					if (focusComponent != null) {
						focusComponent.requestFocus();
					}
				}
			}
		}).start();
	}

	private ActionListener inputFilesButtonListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser(config.getInDirectory());
			chooser.setMultiSelectionEnabled(true);
			chooser.setFileFilter(new FileNameExtensionFilter("Image files.", ImageUtils.getExtensions()));
			if (chooser.showOpenDialog(MainWindow.this) == JFileChooser.APPROVE_OPTION) {
				if (chooser.getSelectedFiles().length > 2) {
					inputFilesSelected(chooser.getSelectedFiles());
				} else {
					showError("You have to choose at least 3 images!");
				}
			}
		}
	};

	private ActionListener sampleFilesButtonListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			inputFilesSelected(ResourcesUtils.getSampleImages());
		}
	};

	private ActionListener outputDirectoryButtonListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser chooser = new JFileChooser(config.getOutDirectory());
			chooser.setMultiSelectionEnabled(false);
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setAcceptAllFileFilterUsed(false);
			if (chooser.showOpenDialog(MainWindow.this) == JFileChooser.APPROVE_OPTION) {
				config.setOutDirectory(chooser.getSelectedFile());
				outputDirTextField.setText(chooser.getSelectedFile().getAbsolutePath());
			}
		}
	};

	private ActionListener resetButtonListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (midPreviewImage != null) {
				imagePreviewLabel.setIcon(new ImageIcon(midPreviewImage.getImage()));
			}
		}
	};

	private ActionListener binarizeImagesButtonListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent event) {
			final File outputDirectory = new File(outputDirTextField.getText());

			if (selectedFiles.length == 0) {
				showError("You have to select some input files first.");
				return;
			}
			if (!outputDirectory.exists()) {
				showError("Output directory path is incorrect.");

				return;
			}

			runLongTask(new Runnable() {

				@Override
				public void run() {
					try {
						int avg = Integer.valueOf(avgTextField.getText());
						int stdDev = Integer.valueOf(stdDevTextField.getText());
						boolean firstCriteriumOnly = firstCriteriumOnlyCheckBox.isSelected();

						Binarizer.binarize(selectedFiles, outputDirectory, avg, stdDev, firstCriteriumOnly, progressListener);

						int length = selectedFiles.length - 2;
						showInfo(String.format("Successfully binarized %d image%s.", length, (length > 1) ? "s" : ""));
					} catch (BinarizerException e) {
						showError(String.format("Error occurred while binarizing images. %s", e.getMessage()));
                        e.printStackTrace();
					}
				}
			}, (Component) event.getSource(), MainWindow.this);
		}
	};

	private InputVerifier numberInputVerifier = new InputVerifier() {

		@Override
		public boolean verify(JComponent input) {
			if (input instanceof JTextField) {
				try {
					String text = ((JTextField) input).getText();
					Integer integerValue = Integer.valueOf(text);
					return integerValue < 256;
				} catch (NumberFormatException e) {
					return false;
				}
			}
			return true;
		}
	};

	private class ParametersChangeListener implements ChangeListener, KeyListener {

		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e) {
		}

		@Override
		public void keyReleased(KeyEvent e) {
			Integer avg = Integer.valueOf(avgTextField.getText());
			Integer stdDev = Integer.valueOf(stdDevTextField.getText());

			avgSlider.removeChangeListener(this);
			sSlider.removeChangeListener(this);

			if (avg >= avgSlider.getMinimum() && avg <= avgSlider.getMaximum()) {
				avgSlider.setValue(avg);
			}
			if (stdDev >= sSlider.getMinimum() && stdDev <= sSlider.getMaximum()) {
				sSlider.setValue(stdDev);
			}

			avgSlider.addChangeListener(this);
			sSlider.addChangeListener(this);
			boolean firstCriteriumOnly = firstCriteriumOnlyCheckBox.isSelected();

			updateConfig(avg, stdDev, firstCriteriumOnly);

			refreshPreview((Component) e.getSource(), null, avg, stdDev, firstCriteriumOnly);
		}

		@Override
		public void stateChanged(ChangeEvent e) {
			int avg = avgSlider.getValue();
			int stdDev = sSlider.getValue();
			boolean firstCriteriumOnly = firstCriteriumOnlyCheckBox.isSelected();
			avgTextField.setText(avg + "");
			stdDevTextField.setText(stdDev + "");

			updateConfig(avg, stdDev, firstCriteriumOnly);

			refreshPreview((Component) e.getSource(), (Container) e.getSource(), avg, stdDev, firstCriteriumOnly);
		}

		private void updateConfig(int avg, int sigma, boolean firstCriteriumOnly) {
			config.setAvg(avg);
			config.setSigma(sigma);
			config.setFirstCriteriumOnly(firstCriteriumOnly);
		}
	}
}

