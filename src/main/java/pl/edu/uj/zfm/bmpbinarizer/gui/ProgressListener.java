package pl.edu.uj.zfm.bmpbinarizer.gui;

import javax.swing.*;

public class ProgressListener {

	private JProgressBar progressBar;
	private long beginTime;

	public ProgressListener(JProgressBar progressBar) {
		this.progressBar = progressBar;
	}

	public void start(int min, int max) {
		progressBar.setEnabled(true);
		progressBar.setMinimum(min);
		progressBar.setMaximum(max);
		progressBar.setValue(0);
		progressBar.setString("");
		beginTime = System.currentTimeMillis();
	}

	public void setProgress(int value) {
		progressBar.setValue(value);

		int max = progressBar.getMaximum();
		if (value == max) {
			progressBar.setString("Done");
		} else {
			long remaining = getTimeRemaining(value) / 1000 + 1;
			progressBar.setString(String.format("[%d/%d] Time remaining: %d s", value, max, remaining));
		}
	}

	public void finish() {
		progressBar.setEnabled(false);
		progressBar.setValue(0);
		progressBar.setString("");
	}

	private long getTimeRemaining(int currentValue) {
		int stepsLeft = progressBar.getMaximum() - currentValue;
		long timeSpend = System.currentTimeMillis() - beginTime;

		return timeSpend * stepsLeft / currentValue;
	}
}
