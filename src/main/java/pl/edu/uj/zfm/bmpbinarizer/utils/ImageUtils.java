package pl.edu.uj.zfm.bmpbinarizer.utils;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.imaging.ImageFormat;
import org.apache.commons.imaging.ImageFormats;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.io.FilenameUtils;

public class ImageUtils {

	public static String[] getExtensions() {
		ArrayList<String> extensions = new ArrayList<>(Arrays.asList("TIF", "BMP", "JPG"));
		for (ImageFormat format : ImageFormats.values()) {
			extensions.add(format.getExtension());
		}
		return extensions.toArray(new String[extensions.size()]);
	}

	public static ImageFormat getImageFormatByExtension(File file) throws ImageUtilsException {
		String extension = FilenameUtils.getExtension(file.getName()).toLowerCase();
		if ("jpg".equals(extension)) {
			return ImageFormats.JPEG;
		}
		for (ImageFormat format : ImageFormats.values()) {
			if (format.getName().toLowerCase().contains(extension)) {
				return format;
			}
		}
		throw new ImageUtilsException("Unknown extension: " + extension);
	}

	public static void write(BufferedImage image, File file) throws IOException {
		try {
			ImageFormat format = getImageFormatByExtension(file);
			Imaging.writeImage(image, file, format, new HashMap<String, Object>());
		} catch (ImageWriteException | ImageUtilsException e) {
			throw new IOException(e);
		}
	}

	public static BufferedImage read(File file) throws IOException {
		try {
			InputStream inputStream = ResourcesUtils.getInputStream(file);
			return Imaging.getBufferedImage(inputStream);
		} catch (ImageReadException e) {
			throw new IOException(e);
		}
	}

	public static BufferedImage clone(BufferedImage image) {
		BufferedImage clone = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
		Graphics2D graphics = clone.createGraphics();
		graphics.drawImage(image, 0, 0, null);
		graphics.dispose();
		return clone;
	}

	public static BufferedImage resize(BufferedImage image, int areaWidth, int areaHeight) {
		float scaleX = (float) areaWidth / image.getWidth();
		float scaleY = (float) areaHeight / image.getHeight();
		float scale = Math.min(scaleX, scaleY);
		int w = Math.round(image.getWidth() * scale);
		int h = Math.round(image.getHeight() * scale);

		int type = image.getTransparency() == Transparency.OPAQUE ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;

		boolean scaleDown = scale < 1;

		if (scaleDown) {
			// multi-pass bilinear div 2
			int currentW = image.getWidth();
			int currentH = image.getHeight();
			BufferedImage resized = image;
			while (currentW > w || currentH > h) {
				currentW = Math.max(w, currentW / 2);
				currentH = Math.max(h, currentH / 2);

				BufferedImage temp = new BufferedImage(currentW, currentH, type);
				Graphics2D g2 = temp.createGraphics();
				g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				g2.drawImage(resized, 0, 0, currentW, currentH, null);
				g2.dispose();
				resized = temp;
			}
			return resized;
		} else {
			Object hint = scale > 2 ? RenderingHints.VALUE_INTERPOLATION_BICUBIC : RenderingHints.VALUE_INTERPOLATION_BILINEAR;

			BufferedImage resized = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2 = resized.createGraphics();
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
			g2.drawImage(image, 0, 0, w, h, null);
			g2.dispose();
			return resized;
		}
	}
}