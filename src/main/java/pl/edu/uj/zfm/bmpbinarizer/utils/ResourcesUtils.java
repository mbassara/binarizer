package pl.edu.uj.zfm.bmpbinarizer.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ResourcesUtils {

	public static boolean isResource(File file) {
		return getClassLoader().getResource(file.getPath().replace('\\','/')) != null;
	}

	private static ClassLoader getClassLoader() {
		return ResourcesUtils.class.getClassLoader();
	}

	public static InputStream getInputStream(File file) {
		if (isResource(file)) {
			return getClassLoader().getResourceAsStream(file.getPath().replace('\\','/'));
		}
		try {
			return new FileInputStream(file);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static File[] getSampleImages() {
		return new File[] {
				new File("sample_images/06_SC_rec0900.tif"),
				new File("sample_images/06_SC_rec0901.tif"),
				new File("sample_images/06_SC_rec0902.tif"),
				new File("sample_images/06_SC_rec0903.tif"),
				new File("sample_images/06_SC_rec0904.tif"),
				new File("sample_images/06_SC_rec0905.tif"),
				new File("sample_images/06_SC_rec0906.tif"),
				new File("sample_images/06_SC_rec0907.tif"),
				new File("sample_images/06_SC_rec0908.tif"),
				new File("sample_images/06_SC_rec0909.tif"),
				new File("sample_images/06_SC_rec0910.tif"),
				new File("sample_images/06_SC_rec0911.tif"),
				new File("sample_images/06_SC_rec0912.tif"),
				new File("sample_images/06_SC_rec0913.tif"),
				new File("sample_images/06_SC_rec0914.tif"),
				new File("sample_images/06_SC_rec0915.tif"),
				new File("sample_images/06_SC_rec0916.tif"),
				new File("sample_images/06_SC_rec0917.tif"),
				new File("sample_images/06_SC_rec0918.tif"),
				new File("sample_images/06_SC_rec0919.tif"),
				new File("sample_images/06_SC_rec0920.tif"),
				new File("sample_images/06_SC_rec0921.tif"),
				new File("sample_images/06_SC_rec0922.tif"),
				new File("sample_images/06_SC_rec0923.tif"),
				new File("sample_images/06_SC_rec0924.tif"),
				new File("sample_images/06_SC_rec0925.tif"),
				new File("sample_images/06_SC_rec0926.tif"),
				new File("sample_images/06_SC_rec0927.tif"),
				new File("sample_images/06_SC_rec0928.tif"),
				new File("sample_images/06_SC_rec0929.tif"),
				new File("sample_images/06_SC_rec0930.tif")
		};
	}
}
