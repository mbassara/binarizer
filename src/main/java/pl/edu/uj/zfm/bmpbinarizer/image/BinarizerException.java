package pl.edu.uj.zfm.bmpbinarizer.image;

public class BinarizerException extends Exception {
	private static final long serialVersionUID = 1L;

	public BinarizerException(String message, Throwable cause) {
		super(message, cause);
	}

	public BinarizerException(Throwable cause) {
		super(cause.getMessage(), cause);
	}

	public BinarizerException(String message) {
		super(message);
	}

}
