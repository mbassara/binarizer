package pl.edu.uj.zfm.bmpbinarizer.image;

import org.apache.commons.io.FileUtils;
import pl.edu.uj.zfm.bmpbinarizer.gui.ProgressListener;
import pl.edu.uj.zfm.bmpbinarizer.utils.ResourcesUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class Binarizer {

	private static final int BLACK = 0;

	private static final int WHITE = 255;

	public static void binarize(File[] inputFiles, File outputDirectory, int avg, int stdDev, boolean firstCriteriumOnly,
			ProgressListener progressListener) throws BinarizerException {
		if (inputFiles.length < 3) {
			throw new BinarizerException("Binarizer requires at least 3 inputFiles.");
		}

		progressListener.start(0, inputFiles.length - 2);
		for (int i = 1; i + 1 < inputFiles.length; i++) {
			File outputFile = new File(outputDirectory, "bin_" + inputFiles[i].getName());
			binarize(inputFiles[i + 1], inputFiles[i], inputFiles[i - 1], outputFile, avg, stdDev, firstCriteriumOnly);
			progressListener.setProgress(i);
		}
		progressListener.finish();
	}

    private static void binarize(File topFile, File midFile, File bottomFile, File outputFile, int avg, int stdDev,
			boolean firstCriteriumOnly)
			throws BinarizerException {
		try {
            GrayscaleImage topImage = new GrayscaleImage(topFile);
            GrayscaleImage midImage = new GrayscaleImage(midFile);
            GrayscaleImage bottomImage = new GrayscaleImage(bottomFile);
            if (isSampleFile(midFile)) {
                copyToOutputDirectory(outputFile.getParentFile(), midFile);
            }
			binarize(topImage, midImage, bottomImage, avg, stdDev, firstCriteriumOnly).save(outputFile);
		} catch (IOException e) {
			throw new BinarizerException(e);
		}
	}

    private static boolean isSampleFile(File file) {
        return file.getAbsolutePath().contains("sample_images");
    }

    private static void copyToOutputDirectory(File outputDirectory, File inputFile) throws IOException {
        File dir = new File(outputDirectory, "input_images");
        File copy = new File(dir, inputFile.getName());
        dir.mkdir();

        try (InputStream stream = ResourcesUtils.getInputStream(inputFile)) {
            FileUtils.copyInputStreamToFile(stream, copy);
        }
    }

    public static GrayscaleImage binarize(GrayscaleImage topImage, GrayscaleImage midImage, GrayscaleImage bottomImage, int avg,
                                          int stdDev, boolean firstCriteriumOnly) throws BinarizerException {
		return new Binarizer(topImage, midImage, bottomImage, avg, stdDev, firstCriteriumOnly)._binarize();
	}

    private final GrayscaleImage topImage;

    private final GrayscaleImage midImage;

    private final GrayscaleImage bottomImage;

	private final int[][] pixelMatrix;

	private final int avg;

	private final int stdDev;

	private final boolean firstCriteriumOnly;

    public Binarizer(GrayscaleImage topImage, GrayscaleImage midImage, GrayscaleImage bottomImage, int avg, int stdDev,
			boolean firstCriteriumOnly)
			throws BinarizerException {
		boolean correctHeight = topImage.getHeight() == midImage.getHeight() && midImage.getHeight() == bottomImage.getHeight();
		boolean correctWidth = topImage.getWidth() == midImage.getWidth() && midImage.getWidth() == bottomImage.getWidth();
		if (!correctHeight || !correctWidth) {
			throw new BinarizerException("Input images have to be of the same size!");
		}

		this.topImage = topImage;
		this.midImage = midImage;
		this.bottomImage = bottomImage;
		this.pixelMatrix = createPixelMatrix(midImage);
		this.avg = avg;
		this.stdDev = stdDev;
		this.firstCriteriumOnly = firstCriteriumOnly;
	}

	private GrayscaleImage _binarize() {
		for (int x = 0; x < midImage.getWidth(); x++) {
			for (int y = 0; y < midImage.getHeight(); y++) {
				binarizePixel(x, y);
			}
		}
		return createOutputImage();
	}

	private void binarizePixel(int x, int y) {
		int pixelValue = midImage.getPixel(x, y);

		if (checkFirstCriterium(pixelValue)) {
			pixelMatrix[x][y] = WHITE;
		} else if (!firstCriteriumOnly && checkSecondCriterium(pixelValue) && checkNeighbours(x, y)) {
			pixelMatrix[x][y] = WHITE;
		} else {
			pixelMatrix[x][y] = BLACK;
		}
	}

	private boolean checkNeighbours(int x, int y) {
		boolean topNeighbour = checkSecondCriterium(topImage.getPixel(x, y));
		boolean bottomNeighbour = checkSecondCriterium(bottomImage.getPixel(x, y));
		boolean rightNeighbour = true, leftNeighbour = true, backNeighbour = true, frontNeighbour = true;

		if (x + 1 < midImage.getWidth()) {
			rightNeighbour = checkSecondCriterium(midImage.getPixel(x + 1, y));
		}
		if (x - 1 >= 0) {
			leftNeighbour = checkSecondCriterium(midImage.getPixel(x - 1, y));
		}
		if (y + 1 < midImage.getHeight()) {
			backNeighbour = checkSecondCriterium(midImage.getPixel(x, y + 1));
		}
		if (y - 1 >= 0) {
			frontNeighbour = checkSecondCriterium(midImage.getPixel(x, y - 1));
		}

		return topNeighbour && bottomNeighbour && rightNeighbour && leftNeighbour && backNeighbour && frontNeighbour;
	}

	private boolean checkFirstCriterium(int pixelValue) {
		return (avg - stdDev <= pixelValue) && (pixelValue <= avg + stdDev);
	}

	private boolean checkSecondCriterium(int pixelValue) {
		return (avg - 2 * stdDev <= pixelValue) && (pixelValue <= avg + 2 * stdDev);
	}

    private GrayscaleImage createOutputImage() {
        GrayscaleImage outputImage = midImage.copy();
		for (int x = 0; x < outputImage.getWidth(); x++) {
			for (int y = 0; y < outputImage.getHeight(); y++) {
				outputImage.setPixel(x, y, pixelMatrix[x][y]);
			}
		}
		return outputImage;
	}

    private int[][] createPixelMatrix(GrayscaleImage image) {
		int[][] pixelMatrix = new int[image.getWidth()][];
		for (int x = 0; x < image.getWidth(); x++) {
			pixelMatrix[x] = new int[image.getHeight()];
		}
		return pixelMatrix;
	}
}
