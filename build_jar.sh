#!/bin/bash

mvn clean install -Dmaven.test.skip
JAR_NAME=`ls target/*.jar | head -n 1 | cut -d'/' -f 2`
cp target/*dependencies.jar $JAR_NAME
